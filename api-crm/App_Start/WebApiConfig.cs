﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using api_crm.Controllers;
using log4net.Config;

namespace api_crm
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
        // Configuración y servicios de API web
        XmlConfigurator.Configure();

        config.EnableCors();
        config.Formatters.Insert(0, new TextMediaTypeFormatter());

            // Rutas de API web
        config.MapHttpAttributeRoutes();

        config.Routes.MapHttpRoute(
        name: "DefaultApi",
        routeTemplate: "api/{controller}/{id}",
        defaults: new { id = RouteParameter.Optional }
      );
    }
  }
}
