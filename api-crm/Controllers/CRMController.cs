﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using log4net;
using System.Text;
using System.Net.Http.Formatting;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace api_crm.Controllers
{
    [EnableCors(origins: "https://mas-testa.inconcertcc.com", headers: "*", methods: "*")]
    public class CRMController : ApiController
    {
        public static string _url = System.Configuration.ConfigurationManager.AppSettings.Get("URLCRM");
        public static string _userName = System.Configuration.ConfigurationManager.AppSettings.Get("USERCRM");
        public static string _password = System.Configuration.ConfigurationManager.AppSettings.Get("PASSCRM");
        public static string _clientId = System.Configuration.ConfigurationManager.AppSettings.Get("CLIENTCRM");
        public static string _apiVersion = System.Configuration.ConfigurationManager.AppSettings.Get("VAPICRM");
        public static string _entidad = System.Configuration.ConfigurationManager.AppSettings.Get("ENTIDADCRM");
        public static string _campos = System.Configuration.ConfigurationManager.AppSettings.Get("CAMPOSENTIDADCRM");

        private static readonly ILog log = LogManager.GetLogger(typeof(CRMController));

        [Route("api/CRM/GetCRM")]
        [HttpGet]
        public async Task<string> GetToken()
        {
            log.Info("WS2Testa.GetToken");
            var respuesta = await GetToken(_url, _apiVersion);
            log.Info("Token: " + respuesta);
            return respuesta;
        }
        [Route("api/CRM/contacto-lead-nuevo")]
        [HttpPost]
        public async Task<string> CrearContacto_lead([FromBody] string contacto)
        {
            log.Info("WS2Testa.CrearContacto-lead: " + contacto);
            string respuesta = "KO";
            if (null != contacto)
            {
                respuesta = await EnviarContactoLead(_url, _apiVersion, "eve_contacto_leads", contacto);
            }
            else
            {
                log.Error("Petición sin datos");
            }
            log.Info("CrearContacto-lead: " + respuesta);
            return respuesta;
        }
        [Route("api/CRM/campanya-comercial-nueva")]
        [HttpPost]
        public async Task<string> CrearCampanyaComercial([FromBody] string cpc)
        {
            log.Info("WS2Testa.CrearCampanyaComercial: " + cpc);
            string respuesta = "KO";
            //Test2 test2 = new Test2(test.eve_name, test.eve_origen, test.eve_tipo_inmueble, test.eve_clasificacion, test.eve_nombre, test.eve_apellidos, test.eve_documento_identidad);
            if (null != cpc)
            {
                respuesta = await EnviarCampanyaComercial(_url, _apiVersion, "eve_campanya_comercials", cpc);
            }
            else
            {
                log.Error("Petición sin datos");
            }
            log.Info("CrearCampanyaComercial: " + respuesta);
            return respuesta;
        }
        [Route("api/CRM/inquilino_update")]
        [HttpPost]
        public async Task<string> ActualizarInquilino([FromBody] string body)
        {
            log.Info("WS2Testa.ActualizarInquilino: " + body);
            string respuesta = "KO";
            if (null != body)
            {
                string id = string.Empty;
                JObject envioalCRM = TratarJson(body, out id);
                string value = JsonConvert.SerializeObject(envioalCRM);
                if (null != value)
                {
                    respuesta = await EnviarPatch(_url, _apiVersion, "accounts", id, value);
                }
            }
            else
            {
                log.Error("Petición sin datos");
            }
            log.Info("ActualizarInquilino: " + respuesta);
            return respuesta;
        }
        [Route("api/CRM/GetInquilino/")]
        [HttpGet]
        public async Task<string> GetDataInquilino(string id)
        {
            log.Info("WS2Testa.GetDataInquilino: " + id);
            string respuesta = "KO";
            if (null != id)
            {
                respuesta = await GetInfoInquilino(_url, _apiVersion, id);
            }
            else
            {
                log.Error("Petición sin datos");
            }
            log.Info("GetDataInquilino: " + respuesta);
            return respuesta;
        }
        [Route("api/CRM/GetCampanyaComercial/")]
        [HttpGet]
        public async Task<string> GetDataCampanyaComercial(string id)
        {
            log.Info("WS2Testa.GetDataCampanyaComercial: " + id);
            string respuesta = "KO";
            if (null != id)
            {
                respuesta = await GetInfoCampanyaComercial(_url, _apiVersion, id);
            }
            else
            {
                log.Error("Petición sin datos");
            }
            log.Info("GetDataInquilino: " + respuesta);
            return respuesta;
        }

        private async Task<string> GetInfoCampanyaComercial(string url, string apiVersion, string id)
        {
            string webApiUrl = $"{url}/api/data/v{apiVersion}/";
            string requestUri = $"{webApiUrl}eve_campanya_comercials({id})";
            log.Info("GetInfoInquilino: " + requestUri);
            string resultado = "KO";
            string accessToken = await GetToken(url, apiVersion);
            var authHeader = new AuthenticationHeaderValue("Bearer", accessToken);
            string idLog = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(webApiUrl);
                client.DefaultRequestHeaders.Authorization = authHeader;
                Dictionary<string, string> valores = new Dictionary<string, string>();
                try
                {
                    idLog = InsertWSLog(id, requestUri, string.Empty, "GetInfoCampanyaComercial");
                    HttpResponseMessage retrieveResponse1 = client.GetAsync(requestUri).Result;
                    if (retrieveResponse1.IsSuccessStatusCode)
                    {
                        JObject body = JObject.Parse(retrieveResponse1.Content.ReadAsStringAsync().Result);
                        resultado = JsonConvert.SerializeObject(body);
                    }
                    else
                    {
                        JObject body = JObject.Parse(retrieveResponse1.Content.ReadAsStringAsync().Result);
                        string errorDetail = string.Empty;
                        try
                        {
                            foreach (var fila in body["error"])
                            {
                                errorDetail += fila.ToString() + " ";
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"ERROR en ConsultarAPI: {ex.Message}");
                            return null;
                        }
                        resultado = $"{retrieveResponse1.StatusCode} {errorDetail}";
                    }
                    UpdateResultWSLog(idLog, resultado);
                }
                catch (Exception e)
                {
                    log.Error(e.Message);
                    log.Error(e.InnerException);
                    log.Error(e.StackTrace);
                }

            }
            return resultado;
        }

        private async Task<string> GetInfoInquilino(string url, string apiVersion, string id)
        {
            string webApiUrl = $"{url}/api/data/v{apiVersion}/";
            string campos = "accountid,eve_nif_nie,eve_numero,eve_portal,eve_escalera,eve_piso,eve_puerta,eve_codigopostal,eve_consentimiento_grupo,eve_consentimiento_terceras,eve_origen_consentimiento,eve_fecha_consentimiento,eve_calificativo,eve_descripcioncalificativo,eve_deudatotalinquilino,eve_deudatotalcontrato,eve_deudatotalplandepago,eve_deudatotalanticipo,eve_importe_total_pendiente_vencimiento_plan,eve_descripcionsociedad,name,eve_nombreapellidos,eve_envio_carta_deuda,eve_estado_anterior_deuda,eve_estado_deuda,eve_fecha_cita,eve_fecha_cita_adicional,eve_fecha_promesa_pago,eve_fecha_ultima_cita,eve_fecha_ultima_gestion,eve_gestion_de_impagos,eve_gestion_de_impagos_adicional,eve_gestora_deuda,eve_multiplessociedades,eve_observacion_carta,eve_observacion_gestion,eve_observaciones_deuda,modifiedon,eve_email,eve_nombre,eve_apellidos,eve_telefono1,eve_telefono2,eve_telefono3,eve_tipovia,eve_calle,eve_poblacion";
            string requestUri = $"{webApiUrl}accounts?$filter=name eq {id}&$select={campos}";
            log.Info("GetInfoInquilino: " + requestUri);
            string resultado = "KO";
            string accessToken = await GetToken(url, apiVersion);
            var authHeader = new AuthenticationHeaderValue("Bearer", accessToken);
            string idLog = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(webApiUrl);
                client.DefaultRequestHeaders.Authorization = authHeader;
                Dictionary<string, string> valores = new Dictionary<string, string>();
                try
                {
                    idLog = InsertWSLog(id, requestUri, string.Empty, "GetInfoInquilino");
                    HttpResponseMessage retrieveResponse1 = client.GetAsync(requestUri).Result;
                    if (retrieveResponse1.IsSuccessStatusCode)
                    {
                        JObject body = JObject.Parse(retrieveResponse1.Content.ReadAsStringAsync().Result);
                        resultado = JsonConvert.SerializeObject(body["value"]);
                    }
                    else
                    {
                        JObject body = JObject.Parse(retrieveResponse1.Content.ReadAsStringAsync().Result);
                        string errorDetail = string.Empty;
                        try
                        {
                            foreach (var fila in body["error"])
                            {
                                errorDetail += fila.ToString() + " ";
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"ERROR en ConsultarAPI: {ex.Message}");
                            return null;
                        }
                        resultado = $"{retrieveResponse1.StatusCode} {errorDetail}";
                    }
                    UpdateResultWSLog(idLog, resultado);
                }
                catch (Exception e)
                {
                    log.Error(e.Message);
                    log.Error(e.InnerException);
                    log.Error(e.StackTrace);
                }

            }
            return resultado;
        }
        #region Utils
        private static JObject TratarJson(string json, out string id)
        {
            JObject result = new JObject();
            id = string.Empty;
            JToken ir = JsonConvert.DeserializeObject<JToken>(json);
            JObject objeto = ir as JObject;
            var elementos = objeto.Properties();
            foreach (var item in elementos)
            {
                if (item.Name.Equals("accountid"))
                {
                    id = item.Value.ToString();
                    log.Debug(id);
                }
                else
                {
                    result.Add(item);
                    log.Debug($"{item.Name} {item.Value}");
                }
            }
            return result;
        }
        private async Task<string> EnviarPost(string url, string apiVersion, string entidad, string json)
        {
            string webApiUrl = $"{url}/api/data/v{apiVersion}/";
            string requestUri = $"{webApiUrl}{entidad}";
            log.Info("EnviarPost:" + requestUri);
            string resultado = "KO";
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            string accessToken = await GetToken(url, apiVersion);
            var authHeader = new AuthenticationHeaderValue("Bearer", accessToken);
            string idLog = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(webApiUrl);
                client.DefaultRequestHeaders.Authorization = authHeader;
                try
                {
                    idLog = InsertWSLog(string.Empty, requestUri, json, "Post");
                    HttpResponseMessage retrieveResponse1 = client.PostAsync(requestUri, data).Result;
                    if (retrieveResponse1.IsSuccessStatusCode)
                    {
                        string uriRespuesta = retrieveResponse1.Headers.Location.AbsoluteUri;
                        int posIni = uriRespuesta.IndexOf("(") + 1;
                        int tam = uriRespuesta.Length - 1 - posIni;
                        resultado = uriRespuesta.Substring(posIni, tam);
                    }
                    else
                    {
                        JObject body = JObject.Parse(retrieveResponse1.Content.ReadAsStringAsync().Result);
                        string errorDetail = string.Empty;
                        try
                        {
                            foreach (var fila in body["error"])
                            {
                                errorDetail += fila.ToString() + " ";
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"ERROR en ConsultarAPI: {ex.Message}");
                            return null;
                        }
                        resultado = $"{retrieveResponse1.StatusCode} {errorDetail}";
                    }
                    UpdateResultWSLog(idLog, resultado);
                }
                catch (Exception e)
                {
                    log.Error(e.Message);
                    log.Error(e.InnerException);
                    log.Error(e.StackTrace);
                }

            }
            log.Info(resultado);
            return resultado;
        }
        private async Task<string> EnviarContactoLead(string url, string apiVersion, string entidad, string json)
        {
            string webApiUrl = $"{url}/api/data/v{apiVersion}/";
            string requestUri = $"{webApiUrl}{entidad}";
            log.Info("EnviarPost:" + requestUri);
            string resultado = "KO";
            string json2 = Procesar_promocionid(json, webApiUrl);
            var data = new StringContent(json2, Encoding.UTF8, "application/json");
            string accessToken = await GetToken(url, apiVersion);
            var authHeader = new AuthenticationHeaderValue("Bearer", accessToken);
            string idLog = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(webApiUrl);
                client.DefaultRequestHeaders.Authorization = authHeader;
                try
                {
                    idLog = InsertWSLog(string.Empty, requestUri, json2, "EnviarContactoLead");
                    HttpResponseMessage retrieveResponse1 = client.PostAsync(requestUri, data).Result;
                    if (retrieveResponse1.IsSuccessStatusCode)
                    {
                        string uriRespuesta = retrieveResponse1.Headers.Location.AbsoluteUri;
                        int posIni = uriRespuesta.IndexOf("(") + 1;
                        int tam = uriRespuesta.Length - 1 - posIni;
                        resultado = uriRespuesta.Substring(posIni, tam);
                    }
                    else
                    {
                        JObject body = JObject.Parse(retrieveResponse1.Content.ReadAsStringAsync().Result);
                        string errorDetail = string.Empty;
                        try
                        {
                            foreach (var fila in body["error"])
                            {
                                errorDetail += fila.ToString() + " ";
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"ERROR en ConsultarAPI: {ex.Message}");
                            return null;
                        }
                        resultado = $"{retrieveResponse1.StatusCode} {errorDetail}";
                    }
                }
                catch (Exception e)
                {
                    log.Error(e.Message);
                    log.Error(e.InnerException);
                    log.Error(e.StackTrace);
                }
            }
            log.Info(resultado);
            return resultado;
        }
        private static string Procesar_promocionid(string json, string url)
        {
            JObject jobj = new JObject();
            JToken ir = JsonConvert.DeserializeObject<JToken>(json);
            JObject objeto = ir as JObject;
            string resultado = string.Empty;
            string newKey = "eve_promocion_id@odata.bind";
            string newUrl = $"{url}eve_promocions";
            var elementos = objeto.Properties();
            bool promo = false;
            foreach (var item in elementos)
            {
                if (item.Name.Equals("eve_promocion_id"))
                {
                    jobj[newKey] = $"{newUrl}({item.Value})";
                    promo = true;
                }
                else
                {
                    jobj.Add(item);
                }
            }
            if (!promo)
            {
                jobj[newKey] = null;
            }
            resultado = JsonConvert.SerializeObject(jobj);
            return resultado;
        }
        private async Task<string> EnviarCampanyaComercial(string url, string apiVersion, string entidad, string json)
        {
            string webApiUrl = $"{url}/api/data/v{apiVersion}/";
            string requestUri = $"{webApiUrl}{entidad}";
            log.Info("EnviarPost:" + requestUri);
            string resultado = "KO";
            string json2 = Procesar_campanyacomercial(json, webApiUrl);
            var data = new StringContent(json2, Encoding.UTF8, "application/json");
            string accessToken = await GetToken(url, apiVersion);
            var authHeader = new AuthenticationHeaderValue("Bearer", accessToken);
            string idLog = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(webApiUrl);
                client.DefaultRequestHeaders.Authorization = authHeader;
                try
                {
                    idLog = InsertWSLog(string.Empty, requestUri, json2, "EnviarCampanyaComercial");
                    HttpResponseMessage retrieveResponse1 = client.PostAsync(requestUri, data).Result;
                    if (retrieveResponse1.IsSuccessStatusCode)
                    {
                        string uriRespuesta = retrieveResponse1.Headers.Location.AbsoluteUri;
                        int posIni = uriRespuesta.IndexOf("(") + 1;
                        int tam = uriRespuesta.Length - 1 - posIni;
                        resultado = uriRespuesta.Substring(posIni, tam);
                    }
                    else
                    {
                        JObject body = JObject.Parse(retrieveResponse1.Content.ReadAsStringAsync().Result);
                        string errorDetail = string.Empty;
                        try
                        {
                            foreach (var fila in body["error"])
                            {
                                errorDetail += fila.ToString() + " ";
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"ERROR en ConsultarAPI: {ex.Message}");
                            return null;
                        }
                        resultado = $"{retrieveResponse1.StatusCode} {errorDetail}";
                    }
                    UpdateResultWSLog(idLog, resultado);
                }
                catch (Exception e)
                {
                    log.Error(e.Message);
                    log.Error(e.InnerException);
                    log.Error(e.StackTrace);
                }

            }
            log.Info(resultado);
            return resultado;
        }
        private static string Procesar_campanyacomercial(string json, string url)
        {
            JObject jobj = new JObject();
            JToken ir = JsonConvert.DeserializeObject<JToken>(json);
            JObject objeto = ir as JObject;
            string resultado = string.Empty;
            string account = "eve_inquilino_principal_id@odata.bind";
            string contrato = "eve_codigo_contrato_id@odata.bind";
            string urlAccount = $"/accounts";
            string urlContrato = $"/eve_contratos";

            var elementos = objeto.Properties();
            foreach (var item in elementos)
            {
                switch (item.Name)
                { 
                    case "eve_inquilino_principal_id":
                        jobj[account] = $"{urlAccount}({item.Value})";
                        break;
                    case "eve_codigo_contrato_id":
                        jobj[contrato] = $"{urlContrato}({item.Value})";
                        break;
                    default:
                        jobj.Add(item);
                        break;
                }
            }
            resultado = JsonConvert.SerializeObject(jobj);
            return resultado;
        }
        private async Task<string> EnviarPatch(string url, string apiVersion, string entidad, string id, string json)
        {
            string webApiUrl = $"{url}/api/data/v{apiVersion}/";
            Uri requestUri = new Uri($"{webApiUrl}{entidad}({id})");
            log.Info("EnviarPatch:" + requestUri);
            string resultado = "KO";
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            log.Info($"Datos enviados: {data}");
            string accessToken = await GetToken(url, apiVersion);
            var authHeader = new AuthenticationHeaderValue("Bearer", accessToken);
            string idLog = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(webApiUrl);
                client.DefaultRequestHeaders.Authorization = authHeader;
                Dictionary<string, string> valores = new Dictionary<string, string>();
                try
                {
                    idLog = InsertWSLog(id, requestUri.ToString(), json, "Patch");
                    HttpResponseMessage retrieveResponse1 = await client.PatchAsync(requestUri, data);
                    if (retrieveResponse1.IsSuccessStatusCode)
                    {
                        resultado = "OK";
                    }
                    else
                    {
                        resultado = $"{retrieveResponse1.StatusCode}";
                    }
                    UpdateResultWSLog(idLog, resultado);
                }
                catch (Exception e)
                {
                    log.Error(e.Message);
                    log.Error(e.InnerException);
                    log.Error(e.StackTrace);
                }
            }
            log.Info(resultado);
            return resultado;
        }
        private async Task<string> GetToken(string url, string apiVersion)
        {
            string webApiUrl = $"{url}/api/data/v{apiVersion}/";
            log.Info("GetToken: " + webApiUrl);
            string resultado = "No token";
            try
            {
                var userCredential = new UserCredential(_userName, _password);
                var authParameters = await AuthenticationParameters.CreateFromResourceUrlAsync(new Uri(webApiUrl));
                var authContext = new AuthenticationContext(authParameters.Authority, false);
                var authResult = authContext.AcquireToken(url, _clientId, userCredential);
                resultado = authResult.AccessToken;
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                log.Error(e.InnerException);
                log.Error(e.StackTrace);
            }
            return resultado;
        }
        private string InsertWSLog(string accountid, string url, string data, string method)
        {
            string queryString = "InsertWSLog";
            string response = "KO";
            using (var connection = new SqlConnection() { ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MyDBConn"].ConnectionString })
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(queryString, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@Accountid", SqlDbType.VarChar, 50).Value = accountid;
                    command.Parameters.Add("@Url", SqlDbType.VarChar, 500).Value = url;
                    command.Parameters.Add("@Data", SqlDbType.VarChar).Value = data;
                    command.Parameters.Add("@Method", SqlDbType.VarChar, 50).Value = method;
                    command.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                    try
                    {
                        command.ExecuteNonQuery();
                        response = command.Parameters["@Id"].Value.ToString();
                    }
                    catch (Exception e)
                    {
                        response = e.Message;
                    }
                }
            }
            return response;
        }
        private string UpdateResultWSLog(string idtabla, string responseWS)
        {
            string queryString = "UpdateResultWSLog";
            string response = "KO";
            int idWS = int.Parse(idtabla);
            using (var connection = new SqlConnection() { ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MyDBConn"].ConnectionString })
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(queryString, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@Id", SqlDbType.Int).Value = idWS;
                    command.Parameters.Add("@Result", SqlDbType.VarChar).Value = responseWS;
                    try
                    {
                        command.ExecuteNonQuery();
                        response = "OK";
                    }
                    catch (Exception e)
                    {
                        response = e.Message;
                    }
                }
            }
            return response;
        }
        #endregion
    }

    public class Contacto_lead
    {
        public string eve_name { get; set; }
        public int eve_origen { get; set; }
        public int eve_tipo_inmueble { get; set; }
        public int eve_clasificacion { get; set; }
        public string eve_nombre { get; set; }
        public string eve_apellidos { get; set; }
        public string eve_documento_identidad { get; set; }
        public object eve_telefono { get; set; }
        public object eve_email { get; set; }
        public object eve_codigo_postal { get; set; }
        public object eve_profesion { get; set; }
        public object eve_estado_civil { get; set; }
        public bool eve_empadronado { get; set; }
        public bool eve_vivienda_propiedad { get; set; }
        public object eve_ingresos_mensuales { get; set; }
        public object eve_observaciones { get; set; }
        public object eve_campanya { get; set; }
        public object eve_como_conocido { get; set; }
        public object eve_zona_localidad { get; set; }
        public bool eve_politica_proteccion_datos { get; set; }
        public bool eve_info_comercial_empresa_colaboradora { get; set; }
        public bool eve_info_comercial_empresa_grupo { get; set; }
        public bool eve_info_testa_finalizado_contrato { get; set; }

    }
    public class Campanya_comercial
    {
        public string eve_name { get; set; }
        public int eve_tipo_envio_renovacion { get; set; }
        public DateTime eve_fecha_envio_renovacion_historico { get; set; }
        public object eve_fecha_fin_contrato { get; set; }
        public int eve_tipo_recuperacion { get; set; }
        public int eve_motivo_inquilino { get; set; }
        public int eve_equipo_encargado_recuperacion { get; set; }
        public DateTime eve_fecha_contacto { get; set; }
        public int eve_reasignacion { get; set; }
        public object eve_otros_motivos_i { get; set; }
        public object eve_otros_motivos_ii { get; set; }
        public object eve_poblacion_destino { get; set; }
        public object eve_codigo_postal_destino { get; set; }
        public string eve_comentarios_motivo_finalizacion { get; set; }
        public int eve_estado_recuperacion { get; set; }
        public object eve_fecha_finalizacion_recuperacion { get; set; }
        public string eve_comentarios_estado_recuperacion { get; set; }
        public object eve_cierre_expediente { get; set; }
        public object eve_inquilino_recuperacion_1 { get; set; }
        public object eve_email_recuperacion_inquilino_1 { get; set; }
        public object eve_telefono_recuperacion_1 { get; set; }
        public object eve_email_recuperacion_inquilino_2 { get; set; }
        public object eve_inquilino_recuperacion_2 { get; set; }
        public object eve_telefono_recuperacion_2 { get; set; }
        public object eve_inquilino_recuperacion_3 { get; set; }
        public object eve_email_recuperacion_inquilino_3 { get; set; }
        public object eve_telefono_recuperacion_3 { get; set; }
        public object eve_clasificacion_cambio_vivienda { get; set; }
        public object eve_motivo_no_recuperacion_1 { get; set; }
        public object eve_motivo_no_recuperacion_2 { get; set; }
    }
    public static class HttpClientExtensions
    {
        public static async Task<HttpResponseMessage> PatchAsync(this HttpClient client, Uri requestUri, HttpContent iContent)
        {
            var method = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, requestUri)
            {
                Content = iContent
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.SendAsync(request);
            }
            catch (TaskCanceledException e)
            {
                Console.WriteLine("ERROR: " + e.ToString());
            }

            return response;
        }
    }
    public class TextMediaTypeFormatter : MediaTypeFormatter
    {
        public TextMediaTypeFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/plain"));
        }

        public override Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            var taskCompletionSource = new TaskCompletionSource<object>();
            try
            {
                var memoryStream = new MemoryStream();
                readStream.CopyTo(memoryStream);
                var s = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                taskCompletionSource.SetResult(s);
            }
            catch (Exception e)
            {
                taskCompletionSource.SetException(e);
            }
            return taskCompletionSource.Task;
        }

        public override Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent content, System.Net.TransportContext transportContext, System.Threading.CancellationToken cancellationToken)
        {
            var buff = System.Text.Encoding.UTF8.GetBytes(value.ToString());
            return writeStream.WriteAsync(buff, 0, buff.Length, cancellationToken);
        }

        public override bool CanReadType(Type type)
        {
            return type == typeof(string);
        }

        public override bool CanWriteType(Type type)
        {
            return type == typeof(string);
        }
    }


}
