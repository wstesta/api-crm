﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api_crm.Controllers
{
  public class ValuesController : ApiController
  {
    // GET api/values
    public IEnumerable<string> Get()
    {
      return new string[] { "value1", "value2" };
    }

    // GET api/values/5
    public string Get(int id)
    {
      return "value";
    }

    // POST api/values
    public void Post([FromBody] Test value)
    {
            Console.WriteLine("hi");
    }

    // PUT api/values/5
    public void Put(int id, [FromBody] string value)
    {
    }

    // DELETE api/values/5
    public void Delete(int id)
    {
    }
  }

    public class Test
    {
        public string eve_contacto_leadid { get; set; }
        public string eve_name { get; set; }
        public string eve_origen { get; set; }
        public string eve_tipo_inmueble { get; set; }
        public string eve_clasificacion { get; set; }
        public string eve_nombre { get; set; }
        public string eve_apellidos { get; set; }
        public string eve_documento_identidad { get; set; }
        public string token { get; set; }


    }
    
}
